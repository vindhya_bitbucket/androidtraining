package com.mobtuts;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobtuts.interfaces.ResponseListener;

public class NetWorkManager  {

    private final RequestQueue queue;

    static enum CALL_FROM{
        VOLLEY,
        RETROFIT
    }
    
    public NetWorkManager(Context context){
         queue = Volley.newRequestQueue(context);

    }

    public void fetchNewsList(final ResponseListener listener){
        String url = "https://newsapi.org/v2/everything?q=bitcoin&from=2018-12-08&sortBy=publishedAt&apiKey=251cee4b837343a896d66ac77f5d0901";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("LifeCycle", error.getLocalizedMessage());
            }
        }
        );

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


}
