package com.mobtuts.interfaces;

public interface ResponseListener {
    void onSuccess(String response);
}
