package com.mobtuts.activitylifecycleplus.base;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.mobtuts.NetWorkManager;
import com.mobtuts.activitylifecycleplus.R;
import com.mobtuts.interfaces.ResponseListener;
import com.mobtuts.model.NewsResponse;

import java.util.ArrayList;

public class TutActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UserRecyleViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tut);

        recyclerView = findViewById(R.id.rv_user_list);

       LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

/*
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
*/


        recyclerView.setLayoutManager(layoutManager);

        ArrayList<UserData> list = getUserList();

        adapter = new UserRecyleViewAdapter(TutActivity.this, list);
        recyclerView.setAdapter(adapter);

        NetWorkManager netWorkManager = new NetWorkManager(this);
        netWorkManager.fetchNewsList(new ResponseListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                NewsResponse newsResponse = gson.fromJson(response, NewsResponse.class);
               // Log.d("", response + newsResponse.toString());
            }
        });




    }

    public ArrayList<String> getListData(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("One");
        arrayList.add("Two");
        arrayList.add("Three");
        arrayList.add("One");
        arrayList.add("Two");
        arrayList.add("Three");
        arrayList.add("One");
        arrayList.add("Two");
        arrayList.add("Three");
        arrayList.add("One");
        arrayList.add("Two");
        arrayList.add("Three");

        return arrayList;
    }

    public void setData(){
    }

    public ArrayList<UserData> getUserList(){
        ArrayList<UserData> arrayList = new ArrayList<>();
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));

        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));


        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));


        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));

        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));
        arrayList.add(new UserData("Vindhya", "Singh"));
        arrayList.add(new UserData("Virat", "Kohli"));


        return arrayList;

    }

    public void getDataFromNetwork(){

    }
}
