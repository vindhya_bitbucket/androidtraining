package com.mobtuts.activitylifecycleplus.base;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobtuts.activitylifecycleplus.R;

public class SecondActivity extends AppCompatActivity {

    TextView tvData;

    private EditText etName;

    private Button btnClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        tvData= findViewById(R.id.textView);

        Intent intent = getIntent();

        if(intent !=null && intent.getExtras() != null){
            String name =intent.getExtras().getString("name","defaun");
            tvData.setText(name);
        }

        etName = findViewById(R.id.editText);

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.putExtra("name", etName.getText().toString());
                setResult(RESULT_OK, intent1);
                finish();
            }
        });



    }
}
