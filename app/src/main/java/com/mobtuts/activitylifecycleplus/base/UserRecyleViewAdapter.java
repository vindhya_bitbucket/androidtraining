package com.mobtuts.activitylifecycleplus.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobtuts.activitylifecycleplus.R;

import java.util.ArrayList;

public class UserRecyleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<UserData> arrayList;

    public int TYPE_ONE = 1;
    public int TYPE_TWO = 2;

    public UserRecyleViewAdapter(Context context, ArrayList<UserData> arrayList){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view = null;


        if(type == TYPE_ONE){
            view = inflater.inflate(R.layout.item_list_main_data, viewGroup, false);
            return   new UserViewHolder(view);

        }else {
            view = inflater.inflate(R.layout.item_section, viewGroup, false);
            return   new SecondViewHolder(view);

        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        Log.d("TutPlus", "Position +  "+ position);

        if(holder instanceof UserViewHolder){

            UserViewHolder userViewHolder = (UserViewHolder)holder;

            userViewHolder.tvText1.setText(arrayList.get(position).firstName);
            userViewHolder.tvText2.setText(arrayList.get(position).lastName + position);

            userViewHolder.tvText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Hi "+position, Toast.LENGTH_LONG).show();
                }
            });
        }else {
            SecondViewHolder userViewHolder = (SecondViewHolder) holder;

        }



    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position % 2==0){
            return TYPE_ONE;
        }else {
            return TYPE_TWO;
        }
    }

    class UserViewHolder extends RecyclerView.ViewHolder{

        private TextView tvText1, tvText2;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            tvText1 = itemView.findViewById(R.id.tv_item_text);
            tvText2 = itemView.findViewById(R.id.tv_item_text_2);
        }
    }

    class SecondViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;

        public SecondViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_section);
        }
    }
}
