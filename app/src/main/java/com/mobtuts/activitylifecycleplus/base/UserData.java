package com.mobtuts.activitylifecycleplus.base;

public class UserData {

    public String firstName, lastName;

    public UserData(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
