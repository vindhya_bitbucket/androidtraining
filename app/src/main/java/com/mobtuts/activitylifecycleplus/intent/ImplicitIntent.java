package com.mobtuts.activitylifecycleplus.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.mobtuts.activitylifecycleplus.R;

import java.io.File;

import static android.os.Environment.DIRECTORY_MUSIC;

public class ImplicitIntent extends AppCompatActivity implements View.OnClickListener {

    private Button btnCamera, btnGallery, btnWeb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicit_intent);
        btnCamera = findViewById(R.id.btn_camera);
        btnWeb = findViewById(R.id.btn_web);
        btnGallery = findViewById(R.id.btn_gallery);

        btnCamera.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnWeb.setOnClickListener(this);
    }




    private void openCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 101);

    }

    private void openGallery(){
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setType("image/*");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void openWeb(){
        String url = "http://www.google.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_camera:
                openCamera();
                break;

            case R.id.btn_gallery:
                openGallery();
                break;

            case R.id.btn_web:
                openWeb();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
